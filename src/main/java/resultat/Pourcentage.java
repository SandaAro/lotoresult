/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resultat;

import cache.Chiffres;
import calcul.Proba;
import java.util.List;

/**
 *
 * @author sanda
 */
public class Pourcentage {
    private int tailleResultat;

    public Pourcentage() {
    }
    
    /**
     * additionne tous les pourcentages du chiffre entrant
     * @param chiffre
     * @param pourcentage 
     */
    private void add(String chiffre,int pourcentage) {
        Chiffres instance = Chiffres.getInstance();
        Integer oldValue = instance.get(chiffre).getValue();
        int newPourcentage = oldValue + pourcentage;
        instance.add(chiffre, newPourcentage);
    }
    
    /**
     * ajouter a partir d'un tableau de resultat
     * @param numtirage
     * @param nombreTirage
     * @param resultat 
     */
    public void add (int numtirage,int nombreTirage,String resultat) {
//        compter les tirages
        this.addNombreTirage(numtirage,nombreTirage) ;
        
        Proba proba = new Proba(numtirage, nombreTirage,resultat);
        String[] resultats = proba.getResultat();
        int tailleSep = resultats.length, nbPourcentage = 0;
        for(int i=0; i<tailleSep; i++){
            nbPourcentage = proba.calculer(i);
            this.add(resultats[i], nbPourcentage);
        }
    }
       
    public void divise(){
        Chiffres instance = Chiffres.getInstance();
        instance.divise(this.getTailleResultat());
    }
    
    private int getTailleResultat() {
        return tailleResultat;
    }

    private void setTailleResultat(int tailleResultat) {
        this.tailleResultat += tailleResultat;
    }
    private void addNombreTirage(int numTirage,int nombreTirage) {
        int nbTirage = nombreTirage - numTirage;
        this.setTailleResultat(nbTirage);
    }
    
    @Override
    public String toString() {
        Chiffres instance = Chiffres.getInstance();
        List list = instance.getPourcentage();
        int taille = list.size();
        String lecture = "";
        for(int i=0; i<taille; i++){
            lecture += list.get(i) +"\n";
        }
        return lecture;
    }
}
