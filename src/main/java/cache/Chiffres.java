/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cache;

import constante.Constante;
import calcul.KeyValue;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sanda
 */
public class Chiffres {
    private List<KeyValue> pourcentage;
    
    private void initialize(){
        this.pourcentage = new ArrayList<>();
        int taille = Constante.nombreBoule;
        KeyValue temp;
        for(int i=0; i<taille; i++){
            temp = new KeyValue(String.valueOf(i+1), 0);
            this.pourcentage.add(temp);
        }
    }
    
    public Chiffres(){
        this.initialize();
    }
    private static Chiffres INSTANCE;
    
    public static Chiffres getInstance(){
        if(INSTANCE == null) INSTANCE = new Chiffres();
        return INSTANCE;
    }
    public static void destroy(){
        INSTANCE = null;
    }
    
    private int getIndicePlace(int max){
        int taille = this.pourcentage.size(),i =0;
        for(i=0; i<taille; i++){
            if(this.pourcentage.get(i).getValue() <= max){
                break;
            }
        }
        return i;
    }
    
    /**
     * Rajouter le chiffre en question dans sa place ordonne par son pourcentage
     * @param chiffre
     * @param pourcentage 
     */
    public void add(String chiffre,int pourcentage) {
//        remplacer le pourcentage du chiffre
        this.remove(chiffre);
        
        int indice = this.getIndicePlace(pourcentage);
        KeyValue temp = new KeyValue(chiffre, pourcentage);
        this.pourcentage.add(indice, temp);
    }
    
    private void remove(String chiffre){
        int taille = this.pourcentage.size();
        KeyValue temp = null;
        for(int i=0; i<taille; i++){
            temp = this.pourcentage.get(i);
            if(temp.getKey().equals(chiffre)){
                this.pourcentage.remove(i);
                break;
            }
        }
    }
    
    public KeyValue get(String chiffre){
        int taille = this.pourcentage.size();
        KeyValue retour = null;
        String key ;
        for(int i=0; i<taille; i++){
            key = String.valueOf(i+1);
            if(key.equals(chiffre)){
                retour = this.pourcentage.get(i);
                break;
            }
        }
        return retour;
    }

    public List<KeyValue> getPourcentage() {
        return this.pourcentage;
    }
    
    private void setPourcentage(List<KeyValue> pourcentage) {
        this.pourcentage = pourcentage;
    }
    
    public void divise(int tailleResultat){
        List<KeyValue> newKeyValues = new ArrayList<>();
        if(tailleResultat > 0){
            KeyValue temp = null,
                    newTemp;
            int value = 0, i = 0;
            while(this.pourcentage.size() > 0){
                temp = (KeyValue) this.pourcentage.get(i);
                value = temp.getValue() / tailleResultat;
                newTemp = new KeyValue(temp.getKey(), value);
                newKeyValues.add(newTemp);
                this.pourcentage.remove(i);
            }
        }
        this.setPourcentage(newKeyValues);
    }
}
