/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  sanda
 * Created: 8 août 2021
 */

ALTER TABLE resultatdetails RENAME COLUMN nbtirage TO numtirage;


create or replace
view public.resultatvalide as
select
	resultat.id,
	resultat.daty,
	resultat.entreprise,
	resultat.nombretirage 
from
	resultat
where
	not (resultat.id in (
	select
		a.resultat
	from
		annulation a));

