/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  sanda
 * Created: 5 août 2021
 */

create or replace
function resultatvalidebetween(debut text,fin text) 
returns setof resultatvalide as $$ 
declare nuplet resultatvalide;
begin 
	FOR nuplet IN SELECT * FROM resultatvalide r where
	r.daty between cast(debut as date) and cast(fin as date) LOOP
	RETURN NEXT nuplet;
end loop;
return;
end $$ language plpgsql;
