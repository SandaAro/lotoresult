/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Connection avec postgres
 * @author sanda
 */
public class GetConnection {
    private static final String base = "lotov1";
    private static final String user = "postgres";
    private static final String motdepasse = "1234";
    
    public static final Connection ouvrir() throws ClassNotFoundException, SQLException{
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection("jdbc:postgresql:"+base,user,motdepasse);
    }
}
