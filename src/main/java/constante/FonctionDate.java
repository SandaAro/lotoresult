/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constante;

import java.sql.Date;

/**
 *
 * @author sanda
 */
public class FonctionDate {
    private int firstYear;
    private Date daty;

    public FonctionDate(Date daty) {
        this.initialize();
        this.setDaty(daty);
    }
    
    public FonctionDate(String daty) {
        this.initialize();
        this.setDaty(daty);
    }
    private void initialize(){
        this.setFirstYear(Constante.firstYear);
    }

    public Date getDaty() {
        return daty;
    }

    private void setDaty(Date daty) {
        this.daty = daty;
    }
    private void setDaty(String daty) {
        if(daty != null && !daty.equals(""))
            this.setDaty(Date.valueOf(daty));
    }

    private void setFirstYear(int firstYear) {
        this.firstYear = firstYear;
    }

    private int getFirstYear() {
        return firstYear;
    }
    
    public static String getReqDateBefore(Date daty){
        String requette = "";
        FonctionDate fDaty = new FonctionDate(daty);
        if(fDaty.getDaty() != null){
            int year = fDaty.getDaty().getYear() + fDaty.getFirstYear(),
                month = fDaty.getDaty().getMonth() + 1,
                monthBefore = month - 1,
                day = fDaty.getDaty().getDate();
            String sMonthBefore = year +"-"+ monthBefore +"-"+ day,
                   sMonth = year +"-"+ month +"-"+ day;
            requette = " and daty between cast('"+sMonthBefore+"' as date) and cast('"+sMonth+"' as date) ";
        }
        return requette;
    }
    
}
