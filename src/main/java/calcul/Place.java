/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcul;

import constante.Constante;

/**
 *
 * @author sanda
 */
public class Place {
    private int indice;
    private int nombreTirage;
    private int numtirage;

    /**
     * 
     * @param numtirage premier ou second tirage
     * @param nombreTirage nombre de tirage durant 1 evenement
     */
    public Place(int numtirage,int nombreTirage) {
        this.setNumtirage(numtirage);
        this.setNombreTirage(nombreTirage);
    }

    public int getProbabilite() {
        int totalResultat = this.getNombreTirage() * Constante.nombreResultat;
        int probaParTirage = totalResultat / this.getNumtirage();
        int probaParIndice = probaParTirage - this.getIndice();
        return probaParIndice;
    }
    
    /**
     * @return le nombre de boule tire au sort lors d'un tirage
     * Parametrable dans calcul.Constante.java
     */
    public int getUnivers() {
        return Constante.nombreBoule * this.getNombreTirage();
    }
    
    private int getNombreTirage() {
        return nombreTirage;
    }

    private void setNombreTirage(int nombreTirage) {
        this.nombreTirage = nombreTirage;
    }
    
    private int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    private int getNumtirage() {
        return numtirage;
    }

    private void setNumtirage(int numtirage) {
        this.numtirage = numtirage;
    }
  
}
