/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcul;

/**
 *
 * @author sanda
 */
public class KeyValue {
    private String key;
    private int value;

    public KeyValue(String key,int value){
        this.setKey(key);
        this.setValue(value);
    }
    
    private void setKey(String key) {
        this.key = key;
    }

    private void setValue(int value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "{ " + key + " : " + value + " % }";
    }
    
    
}
